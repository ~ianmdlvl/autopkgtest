.TH adt\-run 1 2007 autopkgtest "Linux Programmer's Manual"
.SH NAME
adt\-run \- test an installed binary package using the package's tests
.SH SYNOPSYS
.B adt\-run
.IR options ...
.B \-\-\-
.I virt\-server
.RI [ virt\-server\-arg ...]
.br
.SH DESCRIPTION
.B adt\-run
is the program for invoking the autopkgtest package testing machinery.

autopkgtest is a facility for testing binary packages, as installed on
a system (such as a testbed system).  The tests are those supplied in
the source package.

adt\-run runs each test supplied by a particular package and reports
the results.  It drives the specified virtualisation regime as
appropriate, and parses the test description metadata, and arranges
for data to be copied to and from the testbed as required.

adt\-run should be invoked (unless options to the contrary are
supplied) in the top level directory of the built source tree, on the
host.  The package should be installed on the testbed.

.SH PROCESSING INSTRUCTIONS
.TP
.BR --built-tree " " \fIdirectory\fR
Specifies that tests from the built source tree
.IR directory
should be run.  Note that the packages that would normally be
installed as a result of \fB@\fR in the tests' \fBDepends\fR field
(which includes the case where the \fBDepends\fR field is not
specified) are \fInot\fR installed.  The caller must explicitly
instruct \fBadt-run\fR to install any relevant packages.
.TP
.BR --source " " \fIdsc\fR
Builds \fIdsc\fR.  The resulting binaries will (by default) be used to
satisfy dependencies.  The tests from that built tree will also be run
(by default).  The ordering is significant: each \fB--source\fR option
should precede options whose dependencies are to be satisfied by the
binaries it produces.
.TP
.BR --unbuilt-tree " " \fIdirectory\fR
Specifies that tests from the unbuilt source tree
.IR directory
should be run.  This is very similar to specifing \fB--source\fR
except that a directory tree (which should be pristine) is supplied,
instead of a source package.
.TP
.BR --binary " " \fIdeb\fR
Specifies that \fIdeb\fR should be used.  By default it will be used
to satisfy dependencies, both during building and testing, but not
necessarily installed.  The ordering is significant, as for
\fB--source\fR.
.TP
.I filename
Bare filename arguments are processed as if
.BR --built-tree ", " --source ", " --unbuilt-tree " or " --binary
was specified; the nature of the argument is guessed from the form of
the filename.  In the case of \fB--built-tree\fR, either the
option must be specified, or the filename must end in a slash; two
slashes at the end are taken to mean \fB--unbuilt-tree\fR.
.SH PROCESSING OPTIONS
These affect modify processing instructions.  Unless stated
otherwise, they affect all subsequent options.
.TP
.BR --paths-testbed | --paths-host
Specifies that subsequent pathnames in command-line arguments refer to
files on the testbed, or on the host, respectively.  The default is
\fB--paths-host\fR.
.TP
.BR --sources-tests | --sources-no-tests
Specifies that the tests in subsequent \fB--source\fR and
\fB--unbuilt-tree\fR arguments should (or should not) be run.
.TP
.BR --built-binaries-filter= \fIpattern\fB,\fIpattern\fB,\fR...
Specifies that only binaries whose package names match one of the
specified patterns should be used; others will be ignored.  This
option applies to subsequent \fB--source\fR and \fB--unbuilt-tree\fR arguments.
.TP
.BR --no-built-binaries
Specifies that all built binaries should be ignored completely;
equivalent to
.BR --built-binaries-filter=_
(since no package name ever contains \fB_\fR).
.TP
.B --binaries=ignore | --binaries=auto | --binaries=install
Specifies that binary package (in subsequently specified
\fB--binary\fR arguments, or resulting from subsequently specified
\fB--source\fR or \fB--unbuilt-tree\fR arguments and not filtered out) should be ignored, used
only to satisfy dependencies, or installed unconditionally,
respectively.  Equivalent to specifying both
.BR --binaries-forbuilds " and " --binaries-fortests .
.TP
.BI --binaries-forbuilds= ...
Like \fB--binaries=\fR but only changes the handling during package
building: packages will be ignored, used for dependencies, or
unconditionally installed, when a source package is built.
.TP
.BI --binaries-fortests= ...
Like \fB--binaries=\fR but only changes the handling during testing:
packages will be ignored, used for dependencies (including as the
package under test), or unconditionally installed, when tests are run
(as a result of \fB--source\fR, \fB--built-tree\fR or \fB--unbuilt-tree\fR).
.SH OTHER OPTIONS
.TP
.BI --output-dir " " \fIoutput-dir\fR
Specifies that stderr and stdout from the tests should be placed in
.IR output-dir .
These files are named
.BI argid- test -stderr
and
.BI argid- test -stdout
for each test
.IR test ,
and
.BR log
for the log transcript.  If no \fIoutput-dir\fR is specified, or the
path is specified to be on the testbed (ie, if \fB--output-dir\fR
follows \fB--paths-testbed\fR), then the \fBlog\fR file is instead
written to the temporary directory \fItmp\fR if one was specified,
or otherwise no separate copy is made.  Note that the log transcript
output will also be sent to \fBadt-run\fR's stderr unless
\fB--quiet\fR is specified.
.TP
.BI --user= user
Run builds and tests as \fIuser\fR on the testbed.  This needs root on
the testbed; if root on the testbed is not available then builds and
tests run as whatever user is provided.
.TP
.BI --gain-root= gain-root
Prefixes
.B debian/rules binary
with
.RB gain-root .
The default is not to use anything, except that if
\fB--user\fR is supplied or root on the testbed is not available the
default is \fBfakeroot\fR.
.TP
.BI --tmp-dir= tmp
Specifies that \fItmp\fR should be used instead of a fresh
temporary directory on the host.  \fItmp\fR will be created if
necessary, and emptied of all of its contents before \fBadt-run\fR
starts, and it will not be cleaned out afterwards.  \fItmp\fR is
not affected by \fB--paths-testbed\fR.
.B NOTE
again that all of the contents of \fItmp\fR will be \fBdeleted\fR.
.TP
.BI --log-file= logfile
Specifies that the trace log should be written to \fIlogfile\fR
instead of to \fBlog\fR in \fIoutput-dir\fR or \fItmp\fR.
\fIlog-file\fR is not affected by \fB--paths-testbed\fR.
.TP
.BI --summary= summary
Specifies that a summary of the outcome should be written to
\fIsummary\fR.  The events in the summary are written to the log
in any case.
\fIsummary\fR is not affected by \fB--paths-testbed\fR.
.TP
.BR --timeout- \fIwhich\fR = \fIseconds\fR
Use a different timeout for operations on or with the testbed.  There
are four timeouts affected by four values of \fIwhich\fR:
.BR short :
supposedly
short operations like setting up the testbed's apt and checking the
state (default: 100s);
.BR install :
installation of packages including dependencies
(default: 3ks);
.BR test :
test runs (default: 10ks); and
.BR build :
builds (default:
100ks).  The value must be specified as an integer number of seconds.
.TP
.BR --timeout-factor =\fIdouble\fR
Multiply all of the default timeouts by the specified factor (see
\fB--timeout-\fR\fIwhich\fR above).  Only the defaults are affected;
explicit timeout settings are used exactly as specified.
.TP
.BR --debug | -d
Include additional debugging information in the trace log.  Each
additional \fB-d\fR increases the debugging level; the current maximum
is \fB-ddd\fR.  If you like to see what's going on, \fR-d\fB or
\fR-dd\fB is recommended.
.TP
.BI --gnupg-home= dir
Uses \fIdir\fR as the \fBGNUPGHOME\fR for local apt archive signing.
The specified directory should not contain keyrings containing other
unrelated keys, since \fBadt-run\fR does not specify to \fBgpg\fR
which keys to use.  The default is
.BR $HOME/.autopkgtest .
\fB--paths-testbed\fR has no effect on this option.
.TP
.B --gnupg-home=fresh
Use a fresh temporary directory and generate fresh keys each run.
This can be very slow and depends on the availability of sufficient
quantities of high-quality entropy.
.TP
.BR -q " | " --quiet
Do not send a copy of \fBadt-run\fR's trace logstream to stderr.  This
option does not affect the copy sent to \fIlogfile\fR,
\fIoutput-dir\fR or \fItmp\fR.  Note that without the trace
logstream it can be very hard to diagnose problems.
.TP
\fB---\fR \fIvirt-server virt-server-arg\fR...
Specifies the virtualisation regime server, as a command and arguments
to invoke.  All the remaining arguments and options after
.B ---
are passed to the virtualisation server program.
.TP
.BI --set-lang= langval
When running commands on the testbed, sets the \fBLANG\fR environment
variable to \fIlangval\fR.  The default in \fBadt-run\fR is to set it
to \fBC\fR.
.TP
.BI --leave-lang
Suppresses the setting by \fBadt-run\fR of \fBLANG\fR on the testbed.
This results in tests and builds using the testbed's own normal
\fBLANG\fR value setting.

.SH OUTPUT FORMAT
During a normal test run, one line is printed for each test.  This
consists of a short string identifying the test, some horizontal
whitespace, and either
.B PASS
or
.BR FAIL " reason"
or
.BR SKIP " reason"
where the pass/fail indication is separated by any reason by some
horizontal whitespace.

The string to identify the test consists of a short alphanumeric
string invented by \fBadt-run\fR to distinguish different command-line
arguments, the \fIargid\fR, followed by a hyphen and the test name.

Sometimes a
.B SKIP
will be reported when the name of the test is not known or not
applicable: for example, when there are no tests in the package, or a
there is a test stanza which contains features not understood by this
version of
.BR adt-run .
In this case
.B *
will appear where the name of the test should be.

If \fBadt-run\fR detects that erroneous package(s) are involved, it
will print the two lines
.BR "blame: " \fIblamed-thing\fR ...
and
.BR "badpkg: " \fImessage\fR.
Here each whitespace-separated \fIblamed-thing\fR is one of
.BI arg: argument
(representing a pathname found in a command line argument),
.BI dsc: package
(a source package name),
.BI deb: package
(a binary package name)
or possibly other strings to be determined.  This indicates which
arguments and/or packages might have contributed to the problem; the
ones which were processed most recently and which are therefore most
likely to be the cause of a problem are listed last.

.SH EXIT STATUS
0	all tests passed
.br
1	unexpected failure (the python interpreter invents this exit status)
.br
2	at least one test skipped
.br
4	at least one test failed
.br
6	at least one test failed and at least one test skipped
.br
8	no tests in this package
.br
12	erroneous package
.br
16	testbed failure
.br
20	other unexpected failures including bad usage

.SH SEE ALSO
\fBadt-virt-chroot\fR(1), \fBadt-virt-xenlvm\fR(1)

.SH BUGS
This tool still lacks some important features and is not very
well-tested.

.SH AUTHORS AND COPYRIGHT
This manpage is part of autopkgtest, a tool for testing Debian binary
packages.  autopkgtest is Copyright (C) 2006-2007 Canonical Ltd and
others.

See \fB/usr/share/doc/autopkgtest/CREDITS\fR for the list of
contributors and full copying conditions.
