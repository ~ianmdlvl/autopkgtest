#!/usr/bin/python
#
# adt-virt-schroot is part of autopkgtest
# autopkgtest is a tool for testing Debian binary packages
#
# autopkgtest is Copyright (C) 2006-2007 Canonical Ltd.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# See the file CREDITS for a full list of credits information (often
# installed as /usr/share/doc/autopkgtest/CREDITS).

import sys
import os
import string
import re as regexp
import grp
import pwd
from optparse import OptionParser

try: our_base = os.environ['AUTOPKGTEST_BASE']+'/lib'
except KeyError: our_base = '/usr/share/autopkgtest/python';
sys.path.insert(1, our_base)

import VirtSubproc as vsp
capabilities = []

def pw_uid(exp_name):
	try:
		return pwd.getpwnam(exp_name).pw_uid
	except KeyError:
		return None

def gr_gid(exp_name):
	try:
		return grp.getgrnam(exp_name).gr_gid
	except KeyError:
		return None

def match(exp_names, ids, extract_id):
	for exp_name in [n for n in exp_names.split(',') if n]:
		if extract_id(exp_name) in ids:
			return True
	return False

def parse_args():
	global schroot, debuglevel

	usage = "%prog [<options>] <schroot>"
	parser = OptionParser(usage=usage)
	pa = parser.add_option
	pe = parser.error

	pa('-d', '--debug', action='store_true', dest='debug');

	(opts,args) = parser.parse_args()
	if len(args) != 1: pe("need exactly one arg, schroot name")

	schroot = args[0]

	info = vsp.execute('schroot --config -c', [schroot],
		downp=False, outp=True)
	cfg = { }
	ignore_re = regexp.compile('\#|\[|\s*$')
	for entry in info.split("\n"):
		if ignore_re.match(entry): continue
		(key,val) = entry.split("=",2)
		cfg[key] = val

	vsp.debuglevel = opts.debug

	if regexp.search('snapshot',cfg['type']):
		capabilities.append('revert')

	if (match(cfg['root-users'], [os.getuid()], pw_uid) or
	    match(cfg['root-groups'], [os.getgid()] + os.getgroups(), gr_gid)):
		capabilities.append('root-on-testbed')

def hook_open():
	global schroot, sessid, downtmp
	sessid = vsp.execute('schroot -b -c',[schroot], downp=False, outp=True)
	vsp.down = ['schroot','-r','-d','/','-c',sessid]
	if 'root-on-testbed' in capabilities: vsp.down += ['-u','root']
	vsp.down += ['--']
	vsp.downkind = 'auxverb'

def hook_downtmp():
	return vsp.downtmp_mktemp()

def hook_revert():
	vsp.downtmp_remove()
	hook_cleanup()
	hook_open()

def hook_cleanup():
	global schroot, sessid, downtmp
	vsp.downtmp_remove()
	vsp.execute('schroot -e -c',[sessid], downp=False)

def hook_forked_inchild():
	pass

def hook_capabilities():
	return capabilities

parse_args()
vsp.main()
